# just get snakeoil certs from ssl-cert Ubuntu package
FROM ubuntu:focal AS builder-certs
RUN apt-get update \
  && apt-get install -y ssl-cert

FROM registry.gitlab.com/les-connecteurs/docker/alpine

EXPOSE 25 465 587

# dabatabse connection configuration (for aliases and mailboxes)
ENV DB_HOST=db
ENV DB_NAME=postgres
ENV DB_USER=postgres
ENV DB_PASSWORD=postgres

# dabatabse connection configuration (for saslauthd)
ENV DB_AUTH_HOST=db
ENV DB_AUTH_NAME=postgres
ENV DB_AUTH_USER=postgres
ENV DB_AUTH_PASSWORD=postgres
ENV DB_AUTH_TIMEOUT=15

# users table and fields configuration
ENV DB_USERS_TABLE=users
ENV DB_USER_COLUMN=userid
ENV DB_PASSWORD_COLUMN=password
ENV DB_PASSWORD_TYPE=crypt

# mailboxes table
ENV DB_MAILBOXES_TABLE=postfix_mailboxes
ENV DB_MAILBOXES_SELECT=mailbox
ENV DB_MAILBOXES_WHERE=userid

# aliases table
ENV DB_ALIASES_SELECT=destination
ENV DB_ALIASES_TABLE=email_aliases
ENV DB_ALIASES_WHERE=alias

# certificates configuration
ENV PATH_CERTIFICATE_PUBLIC=/etc/ssl/certs/ssl-cert-snakeoil.pem
ENV PATH_CERTIFICATE_PRIVATE=/etc/ssl/private/ssl-cert-snakeoil.key
ENV PATH_DHPARAM=/etc/ssl/dhparam

# other configuration
ENV ORIGIN=localhost
ENV DOMAINS=localhost,localhost.localdomain
ENV MAILBOX_TRANSPORT=lmtp:localhost:2003
ENV OPENDKIM_MILTER=inet:localhost:8891
ENV INET_PROTOCOLS=all
ENV LMTP_PROTOCOL_PREFERENCE=ipv6
ENV SMTPD_BANNER='$myhostname ESMTP $mail_name'
ENV MYHOSTNAME=mail.localhost

# install some dependencies
RUN apk add --no-cache \
  cyrus-sasl@connecteurs \
  cyrus-sasl-login@connecteurs \
  cyrus-sasl-plain@connecteurs \
  gettext \
  pam-pgsql@connecteurs \
  postfix \
  postfix-pgsql \
  postfix-policyd-spf-perl

# used to store templates of configuration files
RUN mkdir -p /app/templates/

COPY entrypoint.sh /app/entrypoint.sh
RUN chmod +x /app/entrypoint.sh

COPY --from=builder-certs \
  /etc/ssl/certs/ssl-cert-snakeoil.pem \
  /etc/ssl/certs/ssl-cert-snakeoil.pem
COPY --from=builder-certs \
  /etc/ssl/private/ssl-cert-snakeoil.key \
  /etc/ssl/private/ssl-cert-snakeoil.key

COPY pam_smtp /etc/pam.d/smtp
COPY mail-pam-pgsql.conf /app/templates/
COPY smtpd.conf /etc/sasl2/smtpd.conf
RUN mkdir -p /var/spool/postfix/var/run/saslauthd

COPY dhparam /etc/ssl/dhparam
COPY master.cf /etc/postfix/
COPY main.cf /app/templates/
COPY mailboxes.cf /app/templates/
COPY aliases.cf /app/templates/
COPY aliases /etc/aliases

CMD [ "/app/entrypoint.sh" ]
