# Postfix

Custom Docker image with Postfix.

## License

This program is free software and is distributed under [AGPLv3+ License](./LICENSE).
